<?php

namespace app\modules\api;

use Yii;
use yii\filters\auth\HttpBearerAuth;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::$app->request->parsers = [
                'application/json' => 'yii\web\JsonParser',
            ];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        if (Yii::$app->controller->action->id !== 'login') {
            $authHeader = \Yii::$app->request->headers->get('Authorization');
            if (preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
                $UUIDv4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
                if (!preg_match($UUIDv4, $matches[1])) {
                    throw new UnauthorizedHttpException('Ошибка авторизации');
                }
            }

            $behaviors['authenticator'] = [
                'class' => HttpBearerAuth::className(),
            ];
        }
        
        return $behaviors;
    }
}
