<?php

namespace app\modules\api\controllers;

use \yii\rest\Controller;
use app\models\Results;
use app\models\SearchForm;
use Yii;

class FindController extends Controller
{
    public function actionFind(): array
    {
        $model = new SearchForm();
        
        if ($model->load(Yii::$app->request->getBodyParams(), "") && $model->validate()) {
            $items = Results::getData($model->searchText);
            
            return [
                'status' => 'ok',
                'items' => $items ?? []
            ];
        } else {
            return [
                'status' => 'error',
                'errors' => $model->getErrors()
            ];
        }
    }
    
    public function actionList(string $searchText, int $start = 0, int $limit = null): array
    {
        $jsonData = Results::find()
                ->select('result')
                ->filterWhere(['search_text' => $searchText])
                ->asArray()
                ->column();
        
        $items = array_slice(json_decode($jsonData[0]), $start, $limit);
        
        return [
            'status' => 'ok',
            'items' => $items
        ];
    }
    
    public function actionDelete(string $id): array
    {
        $model = Results::findOne([$id]);

        try {
            if ($model->delete()) {
                return ['status' => 'ok'];
            } else {
                return ['status' => 'error', 'errors' => $model->getErrors()];
            }
        } catch (\Throwable $th) {
            return ['status' => 'error', 'errors' => [$th->getMessage()]];
        }
    }
    
    /**
     * @OA\GET(
     *   path="/api/find",
     *   tags={"find"},
     *   summary="Поиск по базе",
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(ref="#/components/parameters/start_in_query_required"),
     *   @OA\Parameter(ref="#/components/parameters/start_in_query"),
     *   @OA\Parameter(ref="#/components/parameters/limit_in_query"),
     *   @OA\Response(response=200,ref="#/components/responses/200"),
     * )
     */

    
    /**
     * @OA\POST(
     *   path="/api/find",
     *   tags={"find"},
     *   summary="Поиск",
     *   security={{"bearerAuth": {}}},
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
             @OA\Schema(ref="#/components/schemas/scheme"),
     *     ),
     *   ),
     *   @OA\Response(response=200,ref="#/components/responses/200"),
     * )
     */
    
    /**
     * @OA\DELETE(
     *   path="/api/find/{id}",
     *   tags={"find"},
     *   summary="Удаление",
     *   security={{"bearerAuth": {}}},
     *   @OA\Parameter(ref="#/components/parameters/id_in_path_required"),
     *   @OA\Response(response=200,ref="#/components/responses/200"),
     * )
     */
    
    /**
     *  @OA\Schema(
     *      schema="scheme",
     *      required={"searchText"},
     *      @OA\Property(property="searchText", type="varchar"),
     *      example={
     *          "searchText": "yii2",
     *      }
     *  ),
     */
}
