<?php

namespace app\modules\api\controllers;

use \yii\rest\Controller;
use app\models\LoginForm;
use app\models\UserApiToken;
use Yii;
use yii\web\UnauthorizedHttpException;
use app\models\User;


class AuthController extends Controller {
    
    /**
     * Авторизация
     * @return type
     */
    public function actionLogin() {
        // Проверяем все данные, если всё ок, возвращаем токен.
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->getBodyParams(), '') && $model->login()) {
            $user = User::findByUsername($model->username);
            if ($user->status === User::STATUS_INACTIVE ) {
                return ['status' => 'error', 'message' => 'Ваша учетная запись заблокирована'];
            }
            $token = $user->addApiToken();
            return ['status' => 'ok', 'id' => $user->id, 'token' => $token, 'title' => $user->title];
        }

        return ['status' => 'error', 'message' => 'Неверный логин или пароль'];
    }

    /**
     * Деавторизация
     * @return type
     * @throws UnauthorizedHttpException
     */
    public function actionLogout() {
        if (!Yii::$app->user->isGuest) {
            $token = substr(Yii::$app->request->headers['authorization'], 7, 36);
            UserApiToken::deleteAll(['token' => $token, 'user' => Yii::$app->user->id]);
            
            return ['status' => 'ok'];
        }
        
        throw new UnauthorizedHttpException('Недостаточно прав.');
    }
    
    /**
    * @OA\Info(title="Test", version="1.0.1")
    */
    
    /**
    * @OA\SecurityScheme(
    *   securityScheme="bearerAuth",
    *   in="header",
    *   name="Authorization",
    *   type="http",
    *   scheme="bearer",
    *   bearerFormat="JWT",
    * )
    * 
    * 
    * @OA\Parameter(
    *   parameter="start_in_query_required",
    *   name="searchText",
    *   description="Текст поиска",
    *   @OA\Schema(
    *      type="string",
    *   ),
    *   in="query",
    *   required=true
    * )
    * 
    * 
    * @OA\Parameter(
    *   parameter="start_in_query",
    *   name="start",
    *   description="Выводить записи с ...",
    *   @OA\Schema(
    *      type="string",
    *   ),
    *   in="query"
    * )
    * 
    * @OA\Parameter(
    *   parameter="limit_in_query",
    *   name="limit",
    *   description="Количество возвращаемых записей",
    *   @OA\Schema(
    *      type="string",
    *   ),
    *   in="query"
    * )
    * 
    * @OA\Parameter(
    *   parameter="id_in_path_required",
    *   name="id",
    *   description="ID записи",
    *   @OA\Schema(
    *      type="string",
    *   ),
    *   in="path",
    *   required=true
    * )
    * 
    */

   /** 
    * @OA\Response(
    *   response="200", 
    *   description="Успешно",
    *   @OA\MediaType(
    *      mediaType="application/json",
    *      @OA\Schema(
    *          required={"status"},
    *          @OA\Property(
    *              property="status", 
    *              type="string"),
    *          @OA\Property(
    *              property="items", 
    *              type="object"),
    *          @OA\Property(
    *              property="errors", 
    *              type="object"),
    *          example={
    *              "status": "ok/error",
    *              "items": {
    *                  "col_servers": 1,
    *                  "cnt": "565",
    *              },
    *              "errors": {
    *                 "id": "Значение «c35464f5-18d9-407a-97b8-b8c73b65e23a» для «ID» уже занято."
    *              }
    *          }),
    *   ),
    * )
   */
    
    /**
     * @OA\POST(
     *   path="/api/auth",
     *   tags={"auth"},
     *   summary="Авторизация ",
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *         @OA\Property(
     *           property="username",
     *           description="Имя пользователя",
     *           type="string",
     *         ),
     *         @OA\Property(
     *           property="password",
     *           description="Пароль",
     *           type="string",
     *         ),
     *         example={"username": "test", "password": "test"}
     *       ),
     *     ),
     *   ),
     *   @OA\Response(response=200,ref="#/components/responses/200"),
     * )
    */
    
    /**
     * @OA\GET(
     *   path="/api/auth",
     *   tags={"auth"},
     *   summary="Выход из системы",
     *   security={
     *     {"bearerAuth": {}}
     *   },
     *   @OA\Response(response=200,ref="#/components/responses/200"),
     * )
    */

}
