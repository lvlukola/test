<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\SearchForm;
use app\models\Results;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {        
        $model = new SearchForm();
        if ($model->load(Yii::$app->request->get(), "") && $model->validate()) {
            $items = (array) Results::getData($model->searchText);
        }
        
        return $this->render('index', [
            'model' => $model,
            'items' => $items ?? []
        ]);
    }

}
