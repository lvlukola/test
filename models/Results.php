<?php

namespace app\models;

use Yii;
use yii\httpclient\Client;

/**
 * This is the model class for table "results".
 *
 * @property string $search_text Строка поиска
 * @property string $result Результат
 */
class Results extends \yii\db\ActiveRecord
{
    const SEARCH_URL = 'https://api.github.com/search/repositories';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'results';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['search_text', 'result'], 'required'],
            [['result'], 'safe'],
            [['search_text'], 'string', 'max' => 100],
            [['search_text'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'search_text' => 'Search Text',
            'result' => 'Result',
        ];
    }
    
    public static function getData($searchText) {
        return (Results::findOne($searchText))->result ?? self::getDataFromGit($searchText);
    }
    
    private static function getDataFromGit($searchText) {
        $client = new Client();
        $response = $client->createRequest()
            ->setHeaders(['user-agent' => 'firefox'])
            ->setUrl(self::SEARCH_URL)
            ->setData(['q' => $searchText])
            ->send();
        if ($response->isOk) {
            $items = self::prepareData($response->data['items']);
            self::saveData($searchText, $items);
            return($items);
        }
        
        return false;
    }
    
    private static function prepareData($data) {
        if ($data) {
            $items = array_map(function($row) {
                return [
                    'url' => $row['url'],
                    'watchers' => $row['watchers'],
                    'stargazers_count' => $row['stargazers_count'],
                    'full_name' => $row['full_name'],
                    'login' => $row['owner']['login'],
                ];
            }, $data);
        }
        
        return $items ?? [];
    }


    private static function saveData($searchText, $items) {
        $result = new Results([
            'search_text' => $searchText,
            'result' => $items
        ]);

        $result->save();
    }
}
