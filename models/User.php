<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username Логин пользователя
 * @property string $title Имя пользователя
 * @property string $password_hash
 * @property string $auth_key
 * @property int $status Статус
 * @property int $created_at Дата создания
 * @property UserApiToken[] $userApiTokens
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'title', 'password_hash'], 'required'],
            [['id'], 'string'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
            [['created_at'], 'integer'],
            [['username', 'title', 'password_hash'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'title' => 'Title',
            'password_hash' => 'Password Hash',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[UserApiTokens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserApiTokens()
    {
        return $this->hasMany(UserApiToken::className(), ['user' => 'id']);
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username/*, 'status' => self::STATUS_ACTIVE*/]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $userTokenModel = UserApiToken::findOne(['token' => $token]);

        if ($userTokenModel) {
            return static::findOne(['id' => $userTokenModel->user, 'status' => self::STATUS_ACTIVE]);
        }
        
        return null;
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    /**
     * Generate user api token
     * @return type
     */
    public function addApiToken(){
        $apiTokensModel = new UserApiToken();
        $apiTokensModel->user = $this->id;
        $apiTokensModel->user_agent = \Yii::$app->request->userAgent;
        
        $apiTokensModel->save();

        return $apiTokensModel->token;
    }
}
