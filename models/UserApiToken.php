<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_api_token".
 *
 * @property string $token
 * @property string $user
 * @property string $create_date Дата создания
 * @property string|null $user_agent Клиентское приложение
 *
 * @property User $user0
 */
class UserApiToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_api_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user'], 'required'],
            [['token', 'user'], 'string'],
            [['create_date'], 'safe'],
            [['user_agent'], 'string', 'max' => 256],
            [['token'], 'unique'],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'token' => 'Token',
            'user' => 'User',
            'create_date' => 'Create Date',
            'user_agent' => 'User Agent',
        ];
    }

    /**
     * Gets query for [[User0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }
}
