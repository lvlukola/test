<?php
use yii\helpers\ArrayHelper;
?>
<div class="card col-lg-4" style="cursor: pointer;" onclick="location.href='<?= ArrayHelper::getValue($item, 'url') ?>';">
    <div class="card-body">
        <p class="float-right">
            <i class="fa fa-eye"> <?= ArrayHelper::getValue($item, 'watchers') ?></i>
            <i class="fa fa-star"> <?= ArrayHelper::getValue($item, 'stargazers_count') ?></i>
        </p>
        <h5 class="card-title"><?= ArrayHelper::getValue($item, 'full_name') ?></h5>
        <p class="card-author float-right"><i><?= ArrayHelper::getValue($item, 'login') ?></i></p>        
    </div>
</div>
