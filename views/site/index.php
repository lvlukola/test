<?php
/* @var $this yii\web\View */

$this->title = 'Тестовое задание';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">

        <div class="row">
            <div class="col-lg-12">
                <form class="example" action="/index/search">
                    <input type="text" placeholder="Введите строку поиска" name="searchText" value="<?= $model->searchText ?? '' ?>">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>

    <div class="body-content">
        <div class="row">
            <?php
                foreach ($items as $item) {
                    echo $this->render('card', ['item' => $item]);
                }
            ?>
        </div>
    </div>
</div>
