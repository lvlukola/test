<?php

use yii\db\Migration;
use app\models\User;

/**
 * Class m211008_042632_user_tables
 */
class m211008_042632_user_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
                
        $this->createTable('{{%user}}', [
            'id' => 'UUID PRIMARY KEY not null default uuid_generate_v4()',
            'username' => $this->string()->notNull()->unique()->comment('Логин пользователя'),
            'title' => $this->string()->notNull()->comment('Имя пользователя'),
            'auth_key' => $this->string(32),
            'password_hash' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10)->comment('Статус'),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('now()')->comment('Дата создания'),
        ]);
        
        $this->addCommentOnTable('{{%user}}', 'Пользователи системы');
        
        //токены пользователей
        $this->createTable('user_api_token', [
            'token' => 'UUID PRIMARY KEY not null default uuid_generate_v4()',
            'user' => 'UUID not null REFERENCES "user"("id") ON UPDATE CASCADE ON DELETE CASCADE',
            'create_date' => $this->timestamp()->notNull()->defaultExpression('now()')->comment('Дата создания'),
            'user_agent' => $this->string(256)->comment('Клиентское приложение'),
        ]);
        
        $this->addCommentOnTable('user_api_token', 'Клиентские токены');
        
        $newUser = new User([
            'username' => 'test',
            'title' => 'Тестовый пользователь',
            'status' => 10
        ]);
        
        $newUser->setPassword('test');
        
        $newUser->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_api_token');        
        $this->dropTable('{{%user}}');
    }

}
