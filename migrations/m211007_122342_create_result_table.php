<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%result}}`.
 */
class m211007_122342_create_result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('results', [
            'search_text'              => 'varchar(100) PRIMARY KEY',
            'result'                   => 'JSONB NOT NULL',
        ]);

        $this->addCommentOnTable('results', 'Результаты поиска');

        $this->addCommentOnColumn('results', 'search_text', 'Строка поиска');
        $this->addCommentOnColumn('results', 'result', 'Результат');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('results');
    }
}
